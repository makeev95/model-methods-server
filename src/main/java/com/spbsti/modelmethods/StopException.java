package com.spbsti.modelmethods;

public class StopException extends RuntimeException {

    private final StopCondition condition;

    StopException(StopCondition condition) {
        super(condition.name());
        this.condition = condition;
    }

    public StopCondition getCondition() {
        return condition;
    }
}
