package com.spbsti.modelmethods;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

public class CalcRequest {

    private Long calcAmount = 100000L; //количество вычислений
    private Long loopsAmount = 10000L; //количество циклов
    private Double accuracy; //точность вычисления функции
    private final Collection<Point> points = new ArrayList<>();

    @NotNull
    private Collection<StopCondition> conditions;
    @NotNull
    Double y1; //координаты исходной точки
    @NotNull
    Double y2;
    @NotNull
    Double y1Min; //околоэстремальная область
    @NotNull
    Double y2Min;
    @NotNull
    Double y1Max;
    @NotNull
    Double y2Max;
    @NotNull
    Double step1; //шаги вычислений
    @NotNull
    Double step2;
    @NotNull
    private CalcMethod method;
    @NotNull
    private Extreme extreme;

    @PostConstruct
    public void init() {
        points.add(new Point(y1, y2, Double.MAX_VALUE));
        if (this.accuracy == null)
            this.accuracy = .1;
        if (this.loopsAmount == null)
            this.loopsAmount = 100L;
        if (this.calcAmount == null)
            this.calcAmount = 100L;
    }

    private CalcRequest() {
    }

    void toggleCalcAmount(double y1, double y2, double val) {
        points.add(new Point(y1, y2, val));
        if (conditions.contains(StopCondition.CALC_AMOUNT) && calcAmount-- == 0)
            throw new StopException(StopCondition.CALC_AMOUNT);
    }

    void toggleLoopsAmount() {
        if (conditions.contains(StopCondition.LOOPS_AMOUNT) && loopsAmount-- == 0)
            throw new StopException(StopCondition.LOOPS_AMOUNT);
    }

    void toggleAccuracy(double prev, double val) {
        if (conditions.contains(StopCondition.ACCURACY) && Math.abs(prev - val) < accuracy)
            throw new StopException(StopCondition.ACCURACY);
    }


    public Long getCalcAmount() {
        return calcAmount;
    }

    public void setCalcAmount(Long calcAmount) {
        this.calcAmount = calcAmount;
    }

    public Long getLoopsAmount() {
        return loopsAmount;
    }

    public void setLoopsAmount(Long loopsAmount) {
        this.loopsAmount = loopsAmount;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Collection<StopCondition> getConditions() {
        return conditions;
    }

    public void setConditions(Collection<StopCondition> conditions) {
        this.conditions = conditions;
    }

    public Double getY1() {
        return y1;
    }

    public void setY1(Double y1) {
        this.y1 = y1;
    }

    public Double getY2() {
        return y2;
    }

    public void setY2(Double y2) {
        this.y2 = y2;
    }

    public Double getY1Min() {
        return y1Min;
    }

    public void setY1Min(Double y1Min) {
        this.y1Min = y1Min;
    }

    public Double getY2Min() {
        return y2Min;
    }

    public void setY2Min(Double y2Min) {
        this.y2Min = y2Min;
    }

    public Double getY1Max() {
        return y1Max;
    }

    public void setY1Max(Double y1Max) {
        this.y1Max = y1Max;
    }

    public Double getY2Max() {
        return y2Max;
    }

    public void setY2Max(Double y2Max) {
        this.y2Max = y2Max;
    }

    public Double getStep1() {
        return step1;
    }

    public void setStep1(Double step1) {
        this.step1 = step1;
    }

    public Double getStep2() {
        return step2;
    }

    public void setStep2(Double step2) {
        this.step2 = step2;
    }

    public CalcMethod getMethod() {
        return method;
    }

    public void setMethod(CalcMethod method) {
        this.method = method;
    }

    public Extreme getExtreme() {
        return extreme;
    }

    public void setExtreme(Extreme extreme) {
        this.extreme = extreme;
    }

    public Collection<Point> getPoints() {
        return points;
    }
}
