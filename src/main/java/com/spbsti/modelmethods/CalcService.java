package com.spbsti.modelmethods;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@SuppressWarnings("InfiniteLoopStatement")
@Service
public class CalcService {

    CalcResponse calc(CalcRequest req) {
        //отправка клиенту сообщения об ошибке
        if (req.getConditions().size() < 1)
            throw new RestException("No one stop condition is specified", HttpStatus.BAD_REQUEST);

        switch (req.getMethod()) {
            case VARIABLE_VARIATION:
                return variableVariation(req);
            case FAST_DOWN:
                return fastDown(req);
            case FULL_ITERATION:
                return fullIteration(req);
        }
        return null;
    }

    /*метод поочередного варьирования 1.9603200460356394*/
    private CalcResponse variableVariation(CalcRequest req) {
        Order order = Order.Y1;
        StopCondition cnd = null;
        double extremeVal = req.getExtreme() == Extreme.MIN ? Double.MAX_VALUE : Double.MIN_VALUE;

        try {
            /*нахождение околоэкстремальной области путем поочередного изменения координат*/
            for (; ; req.toggleLoopsAmount()) {
                if (y1Bounds(req.y1.floatValue(), req) && y2Bounds(req.y2.floatValue(), req)) break;
                switch (order) {
                    case Y1:
                        if (req.y1 < req.y2Min) {
                            req.y1 += req.step1;
                        }
                        break;
                    case Y2:
                        if (req.y2 > req.y1Max) {
                            req.y2 -= req.step2;
                        }
                        break;
                }
                order = order == Order.Y1 ? Order.Y2 : Order.Y1;
            }

            for (; ; req.toggleLoopsAmount()) {

                if (y1Bounds(req.y1.floatValue(), req) && y2Bounds(req.y2.floatValue(), req)) { //variables in bounds
                    double val = func(req.y1, req.y2, req); //function value
                    if (req.getExtreme() == Extreme.MIN ? val < extremeVal : val > extremeVal) {
                        req.toggleAccuracy(val, extremeVal);
                        extremeVal = val; //good direction
                        switch (order) {
                            case Y1:
                                req.y1 += req.step1;
                                break;
                            case Y2:
                                req.y2 -= req.step1;
                                break;
                        }
                    } else {
                        //go back
                        req.y1 -= req.step1;
                        req.y2 -= req.step1;
                        req.step1 /= 2;
                        req.step2 /= 2;
                    }
                    order = order == Order.Y1 ? Order.Y2 : Order.Y1;

                } else {
                    break;
                }
            }
        } catch (StopException e) {
            cnd = e.getCondition();
        }

        return new CalcResponse(extremeVal, cnd, req.getPoints());
    }

    /*метод наискорейшего спуска 1.8593289641172215*/
    private CalcResponse fastDown(CalcRequest req) {

        Direction horizontal = Direction.RIGHT;
        Direction vertical = Direction.BOTTOM;
        StopCondition cnd;

        /*вычисление значений частных производных*/
        double dfY1 = dfY1(req.y1, req.y2, req);
        double dfY2 = dfY2(req.y1, req.y2, req);
        double stepDirection;
        Order order;

        if (req.getExtreme() == Extreme.MAX ? dfY1 < dfY2 : dfY2 > dfY2) {
            order = Order.Y1;
            stepDirection = req.step1;
        } else {
            order = Order.Y2;
            stepDirection = req.step2;
        }

        double extremeVal = req.getExtreme() == Extreme.MIN ? Double.MAX_VALUE : Double.MIN_VALUE;

        final int maxChangeCount = 5;
        Integer changeCount = maxChangeCount;


        try {
            for (; ; req.toggleLoopsAmount()) {
                double val = func(req.y1, req.y2, req);
                req.toggleAccuracy(extremeVal, val);

                if (req.getExtreme() == Extreme.MIN ? val < extremeVal : val > extremeVal) {
                    //good direction
                    switch (order) {
                        case Y1:
                            req.y1 = req.y1 + (horizontal == Direction.RIGHT ? stepDirection : -stepDirection);
                            break;
                        case Y2:
                            req.y2 = req.y2 + (vertical == Direction.TOP ? stepDirection : -stepDirection);
                    }
                    extremeVal = val;
                } else {
                    if (changeCount-- == 0) {
                        //calc df
                        dfY1 = dfY1(req.y1, req.y2, req);
                        dfY2 = dfY2(req.y1, req.y2, req);
                        if (req.getExtreme() == Extreme.MAX ? dfY1 < dfY2 : dfY2 > dfY2) {
                            order = Order.Y1;
                            stepDirection = req.y1;
                        } else {
                            order = Order.Y2;
                            stepDirection = req.y2;
                        }
                        changeCount = maxChangeCount;
                    } else {
                        //change direction
                        horizontal = horizontal == Direction.RIGHT ? Direction.LEFT : Direction.RIGHT;
                        vertical = vertical == Direction.BOTTOM ? Direction.TOP : Direction.BOTTOM;
                        stepDirection /= 10;
                    }
                }
            }

        } catch (StopException e) {
            cnd = e.getCondition();
        }

        return new CalcResponse(extremeVal, cnd, req.getPoints());
    }

    /*метод полного перебора 1.8465735902799725*/
    private CalcResponse fullIteration(CalcRequest req) {

        double extremeVal = req.getExtreme() == Extreme.MIN ? Double.MAX_VALUE : Double.MIN_VALUE;
        double iMin = 0;
        double jMin = 0;
        StopCondition cnd;

        try {
            for (double i = req.y1Min; i <= req.y1Max; i += req.step1, req.toggleLoopsAmount()) {
                for (double j = req.y2Min; j <= req.y2Max; j += req.step2, req.toggleLoopsAmount()) {
                    double val = func(i, j, req);
                    req.toggleAccuracy(val, extremeVal);
                    if (req.getExtreme() == Extreme.MIN ? val < extremeVal : val > extremeVal) {
                        extremeVal = val;
                        iMin = i;
                        jMin = j;
                    }
                }
            }

            if (y1Bounds((float) (iMin - req.step1), req)) {
                req.y1Min = iMin - req.step1;
            }
            if (y1Bounds((float) (iMin + req.step1), req)) {
                req.y1Max = iMin + req.step1;
            }

            for (; ; req.toggleLoopsAmount()) {
                req.step1 /= 10;

                for (double i = req.y1Min; i <= req.y1Max; i += req.step1, req.toggleLoopsAmount()) {
                    double val = func(i, jMin, req);
                    req.toggleAccuracy(val, extremeVal);
                    if (req.getExtreme() == Extreme.MIN ? val < extremeVal : val > extremeVal) {
                        extremeVal = val;
                        iMin = i;
                    }
                }

                if (y1Bounds((float) (iMin - req.step1), req)) {
                    req.y1Min = iMin - req.step1;
                }
                if (y1Bounds((float) (iMin + req.step1), req)) {
                    req.y1Max = iMin + req.step1;
                }
            }

        } catch (StopException e) {
            cnd = e.getCondition();
        }

        return new CalcResponse(extremeVal, cnd, req.getPoints());

    }

    private boolean y1Bounds(float y1, CalcRequest req) {
        return y1 >= req.y1Min && y1 <= req.y1Max;
    }

    private boolean y2Bounds(float y2, CalcRequest req) {
        return y2 >= req.y2Min && y2 <= req.y2Max;
    }

    private double func(double y1, double y2, CalcRequest req) {
        double val = Math.pow(y2 - 1, 2) + Math.pow(Math.exp(y1 + y2), 2) - y1 + y2;
        req.toggleCalcAmount(y1, y2, val);
        return val;
    }

    private double dfY1(double y1, double y2, CalcRequest req) {
        double val = Math.pow(Math.exp(y1 + y2), 2) - 1;
        req.toggleCalcAmount(y1, y2, val);
        return val;
    }

    private double dfY2(double y1, double y2, CalcRequest req) {
        double val = Math.pow(Math.exp(y1 + y2), 2) + 2 * y2 - 1;
        req.toggleCalcAmount(y1, y2, val);
        return val;
    }

}
