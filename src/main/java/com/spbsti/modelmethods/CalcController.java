package com.spbsti.modelmethods;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import javax.validation.Valid;

@RestController
@RequestMapping("/calc")
public class CalcController {

    private final CalcService calcService;
    private final RestThreadPool threadPool;

    public CalcController(CalcService calcService, RestThreadPool threadPool) {
        this.calcService = calcService;
        this.threadPool = threadPool;
    }

    @PostMapping
    public DeferredResult<CalcResponse> calc(@RequestBody @Valid CalcRequest req) {
        return threadPool.execute(() -> calcService.calc(req));
    }

}
