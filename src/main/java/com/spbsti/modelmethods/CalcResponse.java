package com.spbsti.modelmethods;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class CalcResponse {
    private Double value;
    private StopCondition stopCondition;
    private Collection<Point> points = new ArrayList<>();

    public CalcResponse(Double value, StopCondition stopCondition, Collection<Point> points) {
        this.value = value;
        this.stopCondition = stopCondition;
        this.points = points;
    }

    public Double getValue() {
        return value;
    }

    public StopCondition getStopCondition() {
        return stopCondition;
    }

    public Collection<Point> getPoints() {
        return points;
    }

    // FIXME: 1/7/18
    public Collection<Double> getY1() {
        return getPoints().stream().map(Point::getY1).collect(Collectors.toList());
    }

    // FIXME: 1/7/18
    public Collection<Double> getY2() {
        return getPoints().stream().map(Point::getY2).collect(Collectors.toList());
    }

    // FIXME: 1/7/18
    public Collection<Double> getVal() {
        return getPoints().stream().map(Point::getVal).collect(Collectors.toList());
    }

}
