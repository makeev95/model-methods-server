package com.spbsti.modelmethods;

/*критерии прекращения поиска*/
public enum StopCondition {
    ACCURACY,
    LOOPS_AMOUNT,
    CALC_AMOUNT
}
