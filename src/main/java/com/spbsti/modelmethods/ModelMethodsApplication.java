package com.spbsti.modelmethods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ModelMethodsApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        builder.profiles("production");
        return builder.sources(ModelMethodsApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ModelMethodsApplication.class, args);
    }
}
