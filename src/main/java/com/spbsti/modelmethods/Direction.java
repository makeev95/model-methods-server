package com.spbsti.modelmethods;

public enum Direction {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}
