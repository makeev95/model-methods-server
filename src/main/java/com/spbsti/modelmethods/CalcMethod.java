package com.spbsti.modelmethods;

public enum CalcMethod {
    VARIABLE_VARIATION,
    FAST_DOWN,
    FULL_ITERATION
}
